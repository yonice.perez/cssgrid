
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>document</title>			
	<link rel="stylesheet" href="/assets/sass/style.css">
</head>
<body>
	<section class="row">
		<div class="cell-content" >
			<h1 class="bold">Campos de texto en un cell content además usa bold type </h1>	
			<p>Ingrese el nombre.....</p>
			<div class="text">					
				<input type="text" />		
				<i class="icon-check"></i>
			</div>

			<div class="text">
				<textarea name=""></textarea>		
				<i class="icon-check visible"></i>
			</div>
		</div>
	</section>

	<section class="row" >
		<div class="cell-content">
			<h1>Subir archivo</h1>	
			<label class="btn">		
				<input type="file" name="archivo"  >
				Subir archivo boton
				<i class="icon-upload right"></i>
			</label>
				
			<hr>	

			<label class="link"><input type="file" name="" value="">Subir archivo<i class="icon-upload"></i></label>
				
			<hr>

			<a href="#" class="link">Es un link</a>	<br>
			<button class="btn btn-primary"> Boton primary</button> <br>
			<button class="btn btn-danger"> Boton danger</button><br>
			<button class="btn btn-success"> Boton success</button><br>
			<button class="btn btn-info"> Boton info</button><br>
			<button class="btn btn-warning"> Boton warning</button><br>		
			<button class="btn btn-primary size-max"> Boton primary</button>
		</div>				
	</section>

	<section class="row">	
		<div class="cell-content">	
			<h1>Este es radio button</h1>	
					
			<label class="type-radio">			
				<input name="opcion" disabled type="radio" name="" value="1"> <div></div> Opción 1	 disabled
			</label>
			<label class="type-radio">	
				<input name="opcion"  readonly="true" type="radio" name="" value="1"> <div></div> Opción 2
			</label>
			<label class="type-radio">
				<input name="opcion" type="radio" name="" value="1"> <div></div> Opción 2
			</label>	

			<h1>Este es checkbox</h1>	
			<label class="type-checkbox">	
				<input type="checkbox" name="" value=""><div></div> check opcion 1
			</label>	
			<label class="type-checkbox">
				<input type="checkbox" name="" value=""><div></div> check opcion 2
			</label>
			<label class="type-checkbox">
				<input type="checkbox" name="" value=""><div></div> check opcion 3
			</label>
		</div>
	</section>

	<section class="row">

		<div class="col-1-2">
			<h2>This is col-1-2</h2>
			<p> 	
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
			</p>
		</div>	
		<div class="col-1-2">
			<h2>This is col-1-2</h2>
			<p> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
			</p>
		</div>	
		<div class="col-1-2">
			<h2>This is col-1-2</h2>
			<p> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
			</p>
		</div>	
	</section>

	<section class="row">

		<div class="col-1-3">
			<h3>This is col-1-3</h3>
			<p> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		</div>	
		<div class="col-1-3">
			<h3>This is col-1-3</h3>
			<p> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
			</p>
		</div>	
		<div class="col-1-3">
			<h3>This is col-1-3</h3>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			<p> 	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.		 
			</p>
		</div>	
	</section>

	<section class="row">	
		<div class="col-extend-1-6">
			<h1>Esto es h1 en columna extendida 1-6</h1>
			<p> ESTAPALABRAESTACONELTEXTODEMASJAJAJJAJAAJAJAJAJAJAJAJ</p>
		</div>		
		<div class="col-extend-5-6">
			<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		
		</div>	
	</section>

	<section class="row">
		<div class="col-3-8">
			<h1>Col-3-8 esta es mas grande vdD?</h1>
			<h2>Este es h2</h2>
			<h3>Este es h3</h3>
			<h5>Este es h4</h4>
			<h5>Este es h5</h5>		
		</div>
		<div class="col-5-8">	
			<h1>Col-5-8 esta es distitna</h1>
			<div style="border:5px solid red;">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>	
			
		</div>
	</section>
</body>
</html>